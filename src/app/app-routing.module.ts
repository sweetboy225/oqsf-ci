import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'walkthrough',
    pathMatch: 'full'
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'walkthrough',
    loadChildren: () => import('./pages/walkthrough/walkthrough.module').then( m => m.WalkthroughPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'otp-confirmation',
    loadChildren: () => import('./pages/otp-confirmation/otp-confirmation.module').then( m => m.OtpConfirmationPageModule)
  },
  {
    path: 'cgu',
    loadChildren: () => import('./pages/cgu/cgu.module').then( m => m.CguPageModule)
  },
  {
    path: 'initier-plainte',
    loadChildren: () => import('./pages/initier-plainte/initier-plainte.module').then( m => m.InitierPlaintePageModule)
  },
  {
    path: 'signin-home',
    loadChildren: () => import('./pages/signin-home/signin-home.module').then( m => m.SigninHomePageModule)
  },
  {
    path: 'edit-profile',
    loadChildren: () => import('./pages/edit-profile/edit-profile.module').then( m => m.EditProfilePageModule)
  },
  {
    path: 'edit-password',
    loadChildren: () => import('./pages/edit-password/edit-password.module').then( m => m.EditPasswordPageModule)
  },
  {
    path: 'details-plainte',
    loadChildren: () => import('./pages/details-plainte/details-plainte.module').then( m => m.DetailsPlaintePageModule)
  },
  {
    path: 'liste-plainte',
    loadChildren: () => import('./pages/liste-plainte/liste-plainte.module').then( m => m.ListePlaintePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

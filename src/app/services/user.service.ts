
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { LoginResponseData } from '../models/login.model';
import { environment } from '../../environments/environment';
import { UserModel } from '../models/user.model';
export class UserService{

    constructor(
        private http: HttpClient
    ){
        
    }

    register(nom, prenoms, sexe, num_tel, pwd, email, id_ville, id_tranche_age){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });

        return this.http.post<string>(environment.BaseUrl + '/?methode=control&option=register/&nom='+ nom + '&prenoms=' + prenoms + '&sexe=' + sexe + '&num_tel='+ num_tel + '&pwd=' +pwd +'&email='+ email + '&id_ville='+ id_ville + '&id_tranche_age' + id_tranche_age,
        {responseType: 'json', headers});
    }

    login(num_tel, pwd){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<LoginResponseData>(environment.BaseUrl + '/?methode=control&option=connection/&num_tel='+ num_tel + '&pwd=' +pwd,
        {responseType: 'json', headers});
    }   

    infos(jwt){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<UserModel>(environment.BaseUrl + '/?methode=control&option=connection/&jwt='+ jwt,
        {responseType: 'json', headers});

    }

    editInfos(nom, prenoms, sexe, email, id_ville, id_tranche_age){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });

        return this.http.post<string>(environment.BaseUrl + '/?methode=control&option=register/&nom='+ nom + '&prenoms=' + prenoms + '&sexe=' + sexe + '&email=' + email + '&id_ville='+ id_ville + '&id_tranche_age' + id_tranche_age,
        {responseType: 'json', headers});
    }

}




import { HttpClient,HttpHeaders } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { EtablissementFinancierModel } from '../models/etablissementFinancier.model';
import { NatureRequeteModel } from '../models/natureRequete.model';
import { StatusModel } from '../models/status.model';
import { TrancheAgeModel } from '../models/trancheAge.model';
import { TypeFichierModel } from '../models/typeFichier.model';
import { TypePLainte } from '../models/typePlainte.model';
import { VilleModel } from '../models/ville.model';
export class ListeService{

    constructor(
        private http: HttpClient
    ){
        
    }

    ville(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<VilleModel>(environment.BaseUrl + '/?methode=liste&option=ville',
        {responseType: 'json', headers});
    } 

    trancheAge(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<TrancheAgeModel>(environment.BaseUrl + '/?methode=liste&option=tranche_age',
        {responseType: 'json', headers});
    } 

    etablissementFinancier(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<EtablissementFinancierModel>(environment.BaseUrl + '/?methode=liste&option=ets_financier',
        {responseType: 'json', headers});
    }

    typePlainte(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<TypePLainte>(environment.BaseUrl + '/?methode=liste&option=type_plainte',
        {responseType: 'json', headers});
    }

    typeFichier(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<TypeFichierModel>(environment.BaseUrl + '/?methode=liste&option=type_fichier',
        {responseType: 'json', headers});
    }

    natureRequete(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<NatureRequeteModel>(environment.BaseUrl + '/?methode=liste&option=nature_requete',
        {responseType: 'json', headers});
    }

    status(){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<StatusModel>(environment.BaseUrl + '/?methode=liste&option=status',
        {responseType: 'json', headers});
    }
}
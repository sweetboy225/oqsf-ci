
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { environment } from '../../environments/environment';
import { EtablissementFinancierModel } from '../models/etablissementFinancier.model';
import { ListePlainteModel } from '../models/listePlainte.model';
import { NatureRequeteModel } from '../models/natureRequete.model';
import { StatusModel } from '../models/status.model';
import { TrancheAgeModel } from '../models/trancheAge.model';
import { TypeFichierModel } from '../models/typeFichier.model';
import { TypePLainte } from '../models/typePlainte.model';
import { VilleModel } from '../models/ville.model';
export class PlainteService{

    constructor(
        private http: HttpClient
    ){
        
    }

    liste(jwt){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<ListePlainteModel>(environment.BaseUrl + '/?methode=liste&option=liste_plaintes&jwt='+ jwt ,
        {responseType: 'json', headers});
    } 

    ajouter(jwt, id_plainte, agence_plainte, desc_plainte, requete_plaignant, date_requete, accuse_reception, solution_propose, date_solution, nature_solution, satisfation_solution, solutionsugerer){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.post<string>(environment.BaseUrl + '/?methode=treatment&option=ajout_plainte&jwt=' + jwt + '&id_plainte=' +id_plainte+ '&agence_plainte=' + agence_plainte +'&desc_plainte=' +desc_plainte + '&requete_plaignant=' +requete_plaignant + '&date_requete=' + date_requete + '&accuse_reception=' +accuse_reception +'&solution_propose=' +solution_propose +'&date_solution=' +date_solution+ '&nature_solution=' + nature_solution +'&satisfation_solution='+ satisfation_solution+ 'solutionsugerer&='+solutionsugerer,
        {responseType: 'json', headers});
    } 

    modifier(jwt, id_etablissement, id_naturerequete, id_typeplainte, agence_plainte, desc_plainte, requete_plaignant, date_requete, accuse_reception, solution_propose, date_solution, nature_solution, satisfation_solution, solutionsugerer){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.post<string>(environment.BaseUrl + '/?methode=treatment&option=ajout_plainte&jwt=' + jwt + '&id_etablissement=' +id_etablissement+ '&id_naturerequete=' + id_naturerequete +'&id_typeplainte=' +id_typeplainte+'&agence_plainte=' + agence_plainte +'&desc_plainte=' +desc_plainte + '&requete_plaignant=' +requete_plaignant + '&date_requete=' + date_requete + '&accuse_reception=' +accuse_reception +'&solution_propose=' +solution_propose +'&date_solution=' +date_solution+ '&nature_solution=' + nature_solution +'&satisfation_solution='+ satisfation_solution+ 'solutionsugerer&='+solutionsugerer,
        {responseType: 'json', headers});
    } 

    deleteFile(jwt, id_fichier){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.get<string>(environment.BaseUrl + '/?methode=treatment&option=del_fichier&jwt='+ jwt+'&id_fichier='+ id_fichier ,
        {responseType: 'json', headers});
    } 
    
    uploadFile(jwt, id_type_ichier, id_plainte, file){
        const headers= new  HttpHeaders({
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        });
        return this.http.post<string>(environment.BaseUrl + '/?methode=upload&option=ajout_fichier&jwt='+jwt+'&id_type_ichier='+ id_type_ichier+'&id_plainte='+id_plainte+'&file='+file,
        {responseType: 'json', headers});
    }

}
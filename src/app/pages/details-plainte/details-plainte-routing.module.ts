import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsPlaintePage } from './details-plainte.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsPlaintePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsPlaintePageRoutingModule {}

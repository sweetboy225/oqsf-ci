import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsPlaintePageRoutingModule } from './details-plainte-routing.module';

import { DetailsPlaintePage } from './details-plainte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsPlaintePageRoutingModule
  ],
  declarations: [DetailsPlaintePage]
})
export class DetailsPlaintePageModule {}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-walkthrough',
  templateUrl: './walkthrough.page.html',
  styleUrls: ['./walkthrough.page.scss'],
})
export class WalkthroughPage implements OnInit {

  constructor(
    public router: Router,
    ) { }

  ngOnInit() {
  }

  startApp() {
    this.router
      .navigateByUrl('/folder/inbox', { replaceUrl: true })
      .then(() =>console.log('Ok'));
  }

}

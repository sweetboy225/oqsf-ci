import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListePlaintePageRoutingModule } from './liste-plainte-routing.module';

import { ListePlaintePage } from './liste-plainte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListePlaintePageRoutingModule
  ],
  declarations: [ListePlaintePage]
})
export class ListePlaintePageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListePlaintePage } from './liste-plainte.page';

describe('ListePlaintePage', () => {
  let component: ListePlaintePage;
  let fixture: ComponentFixture<ListePlaintePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListePlaintePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListePlaintePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

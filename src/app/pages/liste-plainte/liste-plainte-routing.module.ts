import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListePlaintePage } from './liste-plainte.page';

const routes: Routes = [
  {
    path: '',
    component: ListePlaintePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListePlaintePageRoutingModule {}

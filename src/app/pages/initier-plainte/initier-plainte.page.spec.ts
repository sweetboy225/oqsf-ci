import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InitierPlaintePage } from './initier-plainte.page';

describe('InitierPlaintePage', () => {
  let component: InitierPlaintePage;
  let fixture: ComponentFixture<InitierPlaintePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitierPlaintePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InitierPlaintePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

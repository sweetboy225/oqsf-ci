import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InitierPlaintePageRoutingModule } from './initier-plainte-routing.module';

import { InitierPlaintePage } from './initier-plainte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InitierPlaintePageRoutingModule
  ],
  declarations: [InitierPlaintePage]
})
export class InitierPlaintePageModule {}

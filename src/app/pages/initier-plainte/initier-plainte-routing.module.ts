import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InitierPlaintePage } from './initier-plainte.page';

const routes: Routes = [
  {
    path: '',
    component: InitierPlaintePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InitierPlaintePageRoutingModule {}

import { TypeFichierModel } from './typeFichier.model';

export class ListePlainteModel{
    msg: string;
    data: [{
        id_plainte: string;
        agence_plainte: string;
        desc_plainte: string;
        requete_plaignant: number;
        date_saisie: Date;
        heure_saisie: Date;
        date_requete: Date;
        accuse_reception: number;
        date_reception: Date;
        solution_propose: number;
        date_solution: Date;
        nature_solution: number;
        satisfaction_solution: number;
        solutionsugerer: string;
        id_etablissement: number;
        nom_etablissement: string;
        id_naturerequete: number;
        nom_naturerequete: string;
        id_typeplainte: number;
        nom_typeplainte: string;
        actions: number;
        fichiers: TypeFichierModel[]
    }]
}
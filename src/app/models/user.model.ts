export class UserModel{
    msg: string;
    data: {
         id_plaignant: string;
         nom: string;
         prenoms: string;
         sexe: string;
         numtel: string;
         email: string;
         date_creation: Date;
         id_ville: number;
         ville: string;
         id_tranche_age: number;
         tranche_age: string
    }
}
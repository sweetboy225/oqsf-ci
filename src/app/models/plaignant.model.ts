export class PlaignantModel{
    nom: string;
    prenoms: string;
    sexe: string;
    num_tel: number;
    pwd: string;
    email: string;
    id_ville: number;
    id_tranche: number;
}